RSpec.describe Braze do
  describe 'setup' do
    before do
      described_class.clear_config!
    end

    context 'with a valid config object' do
      let(:config) { Braze::Config.allocate }

      before { described_class.setup(config) }

      it 'sets default config' do
        expect(described_class.default_config).to eq(config)
      end
    end

    context 'without a config object' do
      it 'raises ArgumentError' do
        expect { described_class.setup({foo: true}) }.to raise_error(ArgumentError)
      end
    end
  end

  describe 'default_config' do
    subject(:default_config) { described_class.default_config }

    context 'when set' do
      let(:config) { Braze::Config.allocate }

      before do
        described_class.setup(config)
      end

      it 'returns a config' do
        expect(default_config).to be_an_instance_of(Braze::Config)
      end

      it 'returns the config from #setup' do
        expect(default_config).to eq(config)
      end
    end

    context 'when not set' do
      before { described_class.clear_config! }

      it 'raises NoDefaultConfig' do
        expect { default_config }.to raise_error(Braze::NoDefaultConfig)
      end
    end
  end
end
