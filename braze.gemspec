# frozen_string_literal: true

require_relative 'lib/braze/version'

Gem::Specification.new do |spec|
  spec.name = 'braze'
  spec.version = Braze::VERSION
  spec.date = '2020-07-08'
  spec.authors = %w[Chavez]
  spec.email = %w[matthew@el-chavez.me]
  spec.summary = 'Braze REST API rubygem'
  spec.description = 'Interact with the Braze REST API'
  spec.homepage = 'https://gitlab.com/mtchavez/braze'
  spec.licenses = %w[MIT]
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')
  spec.cert_chain = %w[certs/mtchavez.pem]
  spec.signing_key = File.join(Gem.user_home, '.ssh', 'gem-private_key.pem') if $PROGRAM_NAME.end_with?('gem')
  spec.rdoc_options = %w[--charset=UTF-8 --main=README.md]

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/mtchavez/braze'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/mtchavez/braze'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
end
