module Braze
  # NoDefaultConfig - Raised when a default config is not set
  # and is trying to be used
  class NoDefaultConfig < StandardError; end
end
