require 'braze/version'
require 'braze/errors'
require 'braze/config'

module Braze
  module_function

  ##
  # Will set up the default config to use for API calls for ease of use
  #
  # @param config [Braze::Config] - Required Braze::Config object to use for requests
  # @return [Braze::Config]
  # @example
  #   config = Braze::Config.new
  #   Braze.setup(config)
  #
  def setup(config)
    raise ArgumentError unless config.is_a?(Braze::Config)

    @default_config = config
  end

  ##
  #
  # @return [Braze::Config]
  def default_config
    raise Braze::NoDefaultConfig unless @default_config

    @default_config
  end

  ##
  #
  # @private Used in testing to clear config
  #
  private def clear_config!
    @default_config = nil
  end
end
